const mongoose = require('mongoose');


const PhotoSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  image: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
});


const Photo = mongoose.model('Photo', PhotoSchema);
module.exports = Photo;