const fs = require('fs').promises;
const axios = require("axios");
const path = require('path');
const config = require("./config");
const {nanoid} = require('nanoid');

const tryToCreateDir = async dirName => {
  const dirPath = path.join(config.uploadPath, dirName);

  try {
    await fs.access(dirPath);
  } catch (e) {
    await fs.mkdir(dirPath, {recursive: true});
  }
};

module.exports = {
  tryToCreateDir
};