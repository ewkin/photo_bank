import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Home from "./containers/Home/Home";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import {useSelector} from "react-redux";
import {Helmet} from "react-helmet";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Helmet titleTemplate="%s - Gallery"
              defaultTitle="Gallery"/>
      <Switch>
        <ProtectedRoute
          isAllowed={user}
          redirectTo='/login'
          exact
          path="/"
          component={Home}
        />
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;