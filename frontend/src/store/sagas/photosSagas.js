import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import {
  fetchPhotosRequest,
  fetchPhotoDelete,
  fetchPhotosSuccess,
  fetchPhotosFailure,
  createPhotoSuccess,
  createPhotoRequest,
  createPhotoFailure,
} from "../actions/PhotosActions";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";

export function* fetchPhotos({payload: userId}) {
  try {
    let url = '/photos';
    if (userId) {
      url += '?user=' + userId;
    }
    const response = yield axiosApi.get(url);
    yield put(fetchPhotosSuccess(response.data));
  } catch (e) {
    yield put(fetchPhotosFailure());
    NotificationManager.error('Could not fetch Photos')
  }
}

export function* deletePhoto({payload: photoId}) {
  try {
    let url = '/photos';
    yield axiosApi.delete(url + photoId);
    const response = yield axiosApi.get(url);
    yield put(fetchPhotosSuccess(response.data));
    NotificationManager.success('Photo was deleted')
  } catch (e) {
    NotificationManager.error('Could not delete Photo')
    yield put(fetchPhotosFailure());
  }
}


export function* createPhoto({payload: photoData}) {
  try {
    const formData = new FormData();
    Object.keys(photoData).forEach(key => {
      formData.append(key, photoData[key]);
    });

    yield axiosApi.post('/Photos', formData);
    yield put(createPhotoSuccess());
    yield put(historyPush('/'));
    NotificationManager.success('Photo was posted')
  } catch (e) {
    NotificationManager.error('Could not post Photo')
    yield put(createPhotoFailure(e.response.data));
  }
}

const photosSagas = [
  takeEvery(fetchPhotosRequest, fetchPhotos),
  takeEvery(createPhotoRequest, createPhoto),
  takeEvery(fetchPhotoDelete, deletePhoto)
];

export default photosSagas;

