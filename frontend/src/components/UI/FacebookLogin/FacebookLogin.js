import React from 'react';
import {useDispatch} from "react-redux";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import {facebookAppId} from "../../../config";
import {facebookLoginRequest} from "../../../store/actions/usersActions";

const FacebookLogin = () => {
  const dispatch = useDispatch();

  const facebookResponse = response => {
    if (response.id) {
      dispatch(facebookLoginRequest(response));
    }
  }

  return (
    <FacebookLoginButton
      appId={facebookAppId}
      fields="name,email,picture"
      render={props => (
        <Button
          fullWidth
          variant={'outlined'}
          color={'primary'}
          onClick={props.onClick}
          startIcon={<FacebookIcon/>}
        >Login with Facebook</Button>
      )}
      callback={facebookResponse}
    />
  );
};

export default FacebookLogin;